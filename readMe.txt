This repository includes multi-thread and interrupt handling upgrades for SPIM package.

source package SPIM :http://spimsimulator.sourceforge.net/, 
full source code: https://sourceforge.net/p/spimsimulator/code/HEAD/tree/


Spim is a self-contained simulator that runs MIPS32 programs. It reads and 
executes assembly language programs written for this processor. Spim also provides a 
simple debugger and minimal set of operating system services. Spim does not execute 
binary (compiled) programs.


SPIMOS_GTU_2.s file includes produces and consumer problem implementation for testing the upgrades.

After placing files to proper places you can run program with follosing command.

./spim -file SPIMOS_GTU_2.s


You can find detailed instructions about SPIMOS_GTU_2.s in report file.