.data
select: .asciiz "Please select one of the two versions of following producer/consumer\n 1. With no mutex \n 2. With mutex \nSelection:"
err: .asciiz "Provide proper value(1 or 2).\n"

producer1: .asciiz " Producer has started producing. \n"
producer2: .asciiz " Producer producing... \n"
producer3: .asciiz " Producer finished producing. \n"

consumer1: .asciiz " Consumer has started consuming. \n"
consumer2: .asciiz " Consumer consuming... \n"
consumer3: .asciiz " Consumer finished consuming. \n"


.text
.globl main
main:
    # add main thread to thread table
    li $v0,18
    syscall
    
    li	$v0, 4
	la	$a0, select
	syscall

    li	$v0, 5
	syscall

    addi $t2,$v0,0
    # menu input value control
    slt $t0,$v0,3
    beq $t0,$zero,exitErr

    slt $t0,$v0,1
    addi $t1,$zero,1
    # menu input value control
    beq $t0,$t1,exitErr

    # jump the version with respect to user input
    beq $t2,$t1,noMutex
    j withMutex

noMutex:
    # Create thread for producer no mutex
    la $a0,producerNoMutex
    addi $a1,$zero,1
    li $v0,19
    syscall

    # Create thread for consumer no mutex
    la $a0,consumerNoMutex
    addi $a1,$zero,2
    li $v0,19
    syscall 

    j exit

withMutex:
    # Create thread for producer with mutex
    la $a0,producerMutex
    addi $a1,$zero,1
    li $v0,19
    syscall

    # Create thread for consumer with mutex
    la $a0,consumerMutex
    addi $a1,$zero,2
    li $v0,19
    syscall 

exit:
    # join thread1
    addi $a0,$zero,1
    li $v0,21
    syscall 

    # join thread2
    addi $a0,$zero,2
    li $v0,21
    syscall 

    # System call code for exit
    li $v0, 10
    syscall


exitErr:
    li	$v0, 4
	la	$a0, err
	syscall

    li $v0, 10
    syscall


producerNoMutex:
    addi $t2,$zero,500
    addi $t0,$zero,500
    addi $t1,$zero,0

    addi $a0,$zero,0

loop1:
    # producer starts producing 
    la	$a0, producer1
    li	$v0, 4
    syscall

    # producer producing 
    la	$a0, producer2
    li	$v0, 4
    syscall

    # producer finished producing 
    la	$a0, producer3
    li	$v0, 4
    syscall

    addi $t1,$t1,1
    beq $t1,$t0,exit1
    j loop1

exit1:
    li $v0, 20 # System call code for exit
    syscall


consumerNoMutex:
    # initialize how many consume
    addi $t2,$zero,500
    addi $t0,$zero,500
    addi $t1,$zero,0

    addi $a0,$a0,0
loop2:

    # consumer starts consuming 
    la	$a0, consumer1
    li	$v0, 4
    syscall

    # consumer consuming 
    la	$a0, consumer2
    li	$v0, 4
    syscall

    # consumer finished consuming 
    la	$a0, consumer3
    li	$v0, 4
    syscall

    # increase loop variables and control
    addi $t1,$t1,1
    beq $t1,$t0,exit2
    j loop2

exit2:
    li $v0, 20 # System call code for exit thread
    syscall



producerMutex:
    addi $t2,$zero,500
    addi $t0,$zero,500
    addi $t1,$zero,0

loop3:

    # mutex lock
    addi $a0,$zero,1
    li	$v0, 22
    syscall

    # producer starts producing 
    la	$a0, producer1
    li	$v0, 4
    syscall

    # producer producing 
    la	$a0, producer2
    li	$v0, 4
    syscall

    # producer finished producing 
    la	$a0, producer3
    li	$v0, 4
    syscall

    # mutex unlock
    addi $a0,$zero,1
    li	$v0, 23
    syscall

    addi $t1,$t1,1
    beq $t1,$t0,exit3
    j loop3

exit3:


    li $v0, 20 # System call code for exit
    syscall


consumerMutex:
    addi $t2,$zero,500
    addi $t0,$zero,500
    addi $t1,$zero,0

loop4:

    # mutex lock
    addi $a0,$zero,1
    li	$v0, 22
    syscall

    # consumer starts consuming 
    la	$a0, consumer1
    li	$v0, 4
    syscall

    # consumer consuming 
    la	$a0, consumer2
    li	$v0, 4
    syscall

    # consumer finished consuming 
    la	$a0, consumer3
    li	$v0, 4
    syscall

    # mutex unlock
    addi $a0,$zero,1
    li	$v0, 23
    syscall


    addi $t1,$t1,1
    beq $t1,$t0,exit4
    j loop4


exit4:

    li $v0, 20 # System call code for exit
    syscall
