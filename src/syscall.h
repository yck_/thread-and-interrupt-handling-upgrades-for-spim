/* SPIM S20 MIPS simulator.
   Execute SPIM syscalls, both in simulator and bare mode.

   Copyright (c) 1990-2010, James R. Larus.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification,
   are permitted provided that the following conditions are met:

   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

   Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation and/or
   other materials provided with the distribution.

   Neither the name of the James R. Larus nor the names of its contributors may be
   used to endorse or promote products derived from this software without specific
   prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
   GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
   LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
   OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


struct threadEntry{ 
  int threadID; 
  int threadState; /*1-Ready 2-Blocked 3-Running*/
  char threadName[64];
  mem_addr dataCounter; /*data program counter , which is same with main thread*/
  mem_addr stackAdd; /*Stores the stack pointer address*/
  mem_addr pCounter; /*Program Counter*/ 
  mem_addr textPCounter; /*Program counter for text segment*/
  mem_addr textPCounterEnd;
  reg_word threadCCR[4][32],threadCPR[4][32];
  reg_word threadHI,threadLO;
  reg_word registers[R_LENGTH];
};


#define TRUE 1


/* Exported functions. */
void SPIM_timerHandler();
int do_syscall ();
void handle_exception ();

#define PRINT_INT_SYSCALL	1
#define PRINT_FLOAT_SYSCALL	2
#define PRINT_DOUBLE_SYSCALL	3
#define PRINT_STRING_SYSCALL	4

#define READ_INT_SYSCALL	5
#define READ_FLOAT_SYSCALL	6
#define READ_DOUBLE_SYSCALL	7
#define READ_STRING_SYSCALL	8

#define SBRK_SYSCALL		9

#define EXIT_SYSCALL		10

#define PRINT_CHARACTER_SYSCALL	11
#define READ_CHARACTER_SYSCALL	12

#define OPEN_SYSCALL		13
#define READ_SYSCALL		14
#define WRITE_SYSCALL		15
#define CLOSE_SYSCALL		16

#define EXIT2_SYSCALL		17


#define INIT_THREAD_SYSCALL 18
#define CREATE 19
#define THREAD_EXIT 20
#define THREAD_JOIN 21
#define MUTEX_LOCK 22
#define MUTEX_UNLOCK 23
#define RANDOM_NUMBER 24


/*This function adds the main thread information to the
  thread table.*/
void initMainThread();

/*Create new thread and add to thread table*/
void threadCreate();
void switchThread();
void threadExit();

/*Updates the current thread informations in the thread table before switch*/
void updateThread();

/*For thread_join*/
void joinThread();

/* 
  Check if given thread is finished 
    0 - Not finished
    1 - Finished
*/
int isThreadFinished(int tid);

void mutexLock();
void mutexUnlock();