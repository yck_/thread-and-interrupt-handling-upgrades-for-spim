/* SPIM S20 MIPS simulator.
   Execute SPIM syscalls, both in simulator and bare mode.
   Execute MIPS syscalls in bare mode, when running on MIPS systems.
   Copyright (c) 1990-2010, James R. Larus.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without modification,
   are permitted provided that the following conditions are met:

   Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

   Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation and/or
   other materials provided with the distribution.

   Neither the name of the James R. Larus nor the names of its contributors may be
   used to endorse or promote products derived from this software without specific
   prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
   GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
   LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
   OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef _WIN32
#include <unistd.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <string>

#ifdef _WIN32
#include <io.h>
#endif

#include "spim.h"
#include "string-stream.h"
#include "inst.h"
#include "reg.h"
#include "mem.h"
#include "sym-tbl.h"
#include "syscall.h"
#include "data.h"
#include "spim-utils.h"

#include <iostream>
using namespace std;

#ifdef _WIN32
/* Windows has an handler that is invoked when an invalid argument is passed to a system
   call. https://msdn.microsoft.com/en-us/library/a9yf33zb(v=vs.110).aspx

   All good, except that the handler tries to invoke Watson and then kill spim with an exception.

   Override the handler to just report an error.
*/

#include <stdio.h>
#include <stdlib.h>
#include <crtdbg.h>

void myInvalidParameterHandler(const wchar_t* expression,
   const wchar_t* function, 
   const wchar_t* file, 
   unsigned int line, 
   uintptr_t pReserved)
{
  if (function != NULL)
    {
      run_error ("Bad parameter to system call: %s\n", function);
    }
  else
    {
      run_error ("Bad parameter to system call\n");
    }
}

static _invalid_parameter_handler oldHandler;

void windowsParameterHandlingControl(int flag )
{
  static _invalid_parameter_handler oldHandler;
  static _invalid_parameter_handler newHandler = myInvalidParameterHandler;

  if (flag == 0)
    {
      oldHandler = _set_invalid_parameter_handler(newHandler);
      _CrtSetReportMode(_CRT_ASSERT, 0); // Disable the message box for assertions.
    }
  else
    {
      newHandler = _set_invalid_parameter_handler(oldHandler);
      _CrtSetReportMode(_CRT_ASSERT, 1);  // Enable the message box for assertions.
    }
}
#endif
// int producerConsumerArea=0;
int threadCount=0;
int currentRuningThread=0;
/*Program supports maximum 100 thread*/
struct threadEntry* threadTable[100]={NULL};

/*This variable allows to manipulate mutexes
  with respect to mutex id.*/
int mutexList[100]={0};

int joinThreadId=0;
int joinThreadFlag=0;

/*You implement your handler here*/
void SPIM_timerHandler()
{
   // Implement your handler..
   try
   {

    if(threadCount!=1){
      if(threadTable[currentRuningThread]->threadState==3)
        threadTable[currentRuningThread]->threadState=1;
      updateThread();
      switchThread();

      threadTable[currentRuningThread]->threadState=1;
    }


	// throw logic_error( "NotImplementedException\n" );
   }
   catch ( exception &e )
   {
      cerr <<  endl << "Caught: " << e.what( ) << endl;

   };
   
}
/* Decides which syscall to execute or simulate.  Returns zero upon
   exit syscall and non-zero to continue execution. */
int
do_syscall ()
{
#ifdef _WIN32
    windowsParameterHandlingControl(0);
#endif

  /* Syscalls for the source-language version of SPIM.  These are easier to
     use than the real syscall and are portable to non-MIPS operating
     systems. */

  switch (R[REG_V0])
    {
    case PRINT_INT_SYSCALL:
      write_output (console_out, "%d", R[REG_A0]);
      break;

    case PRINT_FLOAT_SYSCALL:
      {
	float val = FPR_S (REG_FA0);

	write_output (console_out, "%.8f", val);
	break;
      }

    case PRINT_DOUBLE_SYSCALL:
      write_output (console_out, "%.18g", FPR[REG_FA0 / 2]);
      break;

    case PRINT_STRING_SYSCALL:
      write_output (console_out, "%s", mem_reference (R[REG_A0]));
      break;

    case READ_INT_SYSCALL:
      {
	static char str [256];

	read_input (str, 256);
	R[REG_RES] = atol (str);
	break;
      }

    case READ_FLOAT_SYSCALL:
      {
	static char str [256];

	read_input (str, 256);
	FPR_S (REG_FRES) = (float) atof (str);
	break;
      }

    case READ_DOUBLE_SYSCALL:
      {
	static char str [256];

	read_input (str, 256);
	FPR [REG_FRES] = atof (str);
	break;
      }

    case READ_STRING_SYSCALL:
      {
	read_input ( (char *) mem_reference (R[REG_A0]), R[REG_A1]);
	data_modified = true;
	break;
      }

    case SBRK_SYSCALL:
      {
	mem_addr x = data_top;
	expand_data (R[REG_A0]);
	R[REG_RES] = x;
	data_modified = true;
	break;
      }

    case PRINT_CHARACTER_SYSCALL:
      write_output (console_out, "%c", R[REG_A0]);
      break;

    case READ_CHARACTER_SYSCALL:
      {
	static char str [2];

	read_input (str, 2);
	if (*str == '\0') *str = '\n';      /* makes xspim = spim */
	R[REG_RES] = (long) str[0];
	break;
      }

    case EXIT_SYSCALL:
    {   
        free(threadTable[0]);
        spim_return_value = 0;
        return (0);
    }
    case EXIT2_SYSCALL:
      spim_return_value = R[REG_A0];	/* value passed to spim's exit() call */
      return (0);

    case OPEN_SYSCALL:
      {
#ifdef _WIN32
        R[REG_RES] = _open((char*)mem_reference (R[REG_A0]), R[REG_A1], R[REG_A2]);
#else
	R[REG_RES] = open((char*)mem_reference (R[REG_A0]), R[REG_A1], R[REG_A2]);
#endif
	break;
      }

    case READ_SYSCALL:
      {
	/* Test if address is valid */
	(void)mem_reference (R[REG_A1] + R[REG_A2] - 1);
#ifdef _WIN32
	R[REG_RES] = _read(R[REG_A0], mem_reference (R[REG_A1]), R[REG_A2]);
#else
	R[REG_RES] = read(R[REG_A0], mem_reference (R[REG_A1]), R[REG_A2]);
#endif
	data_modified = true;
	break;
      }

    case WRITE_SYSCALL:
      {
	/* Test if address is valid */
	(void)mem_reference (R[REG_A1] + R[REG_A2] - 1);
#ifdef _WIN32
	R[REG_RES] = _write(R[REG_A0], mem_reference (R[REG_A1]), R[REG_A2]);
#else
	R[REG_RES] = write(R[REG_A0], mem_reference (R[REG_A1]), R[REG_A2]);
#endif
	break;
      }

    case CLOSE_SYSCALL:
      {
#ifdef _WIN32
	R[REG_RES] = _close(R[REG_A0]);
#else
	R[REG_RES] = close(R[REG_A0]);
#endif
	break;
      }

    case INIT_THREAD_SYSCALL:
        {
      /*Init main thread to the thread table.*/
      initMainThread();
      break;
        }

    case CREATE:
      {
        threadCreate();
        break;
      }

    case THREAD_EXIT:
    {
      threadExit();
      break;
    }

    case THREAD_JOIN:
    {
      joinThread();
      break;
    }
	
	case MUTEX_LOCK:
    {
      mutexLock();
      break;
    }

	case MUTEX_UNLOCK:
    {
      mutexUnlock();
      break;
    }
	
  case RANDOM_NUMBER:
  {   /*Produce random*/
      R[REG_V0]=rand()%R[REG_A0];
    break;
  }
	
	default:
      run_error ("Unknown system call: %d\n", R[REG_V0]);
      break;
    }

#ifdef _WIN32
    windowsParameterHandlingControl(1);
#endif

  return (1);
}


void
handle_exception ()
{
  if (!quiet && CP0_ExCode != ExcCode_Int)
    error ("Exception occurred at PC=0x%08x\n", CP0_EPC);

  exception_occurred = 0;
  PC = EXCEPTION_ADDR;

  switch (CP0_ExCode)
    {
    case ExcCode_Int:
      break;

    case ExcCode_AdEL:
      if (!quiet)
	error ("  Unaligned address in inst/data fetch: 0x%08x\n", CP0_BadVAddr);
      break;

    case ExcCode_AdES:
      if (!quiet)
	error ("  Unaligned address in store: 0x%08x\n", CP0_BadVAddr);
      break;

    case ExcCode_IBE:
      if (!quiet)
	error ("  Bad address in text read: 0x%08x\n", CP0_BadVAddr);
      break;

    case ExcCode_DBE:
      if (!quiet)
	error ("  Bad address in data/stack read: 0x%08x\n", CP0_BadVAddr);
      break;

    case ExcCode_Sys:
      if (!quiet)
	error ("  Error in syscall\n");
      break;

    case ExcCode_Bp:
      exception_occurred = 0;
      return;

    case ExcCode_RI:
      if (!quiet)
	error ("  Reserved instruction execution\n");
      break;

    case ExcCode_CpU:
      if (!quiet)
	error ("  Coprocessor unuable\n");
      break;

    case ExcCode_Ov:
      if (!quiet)
	error ("  Arithmetic overflow\n");
      break;

    case ExcCode_Tr:
      if (!quiet)
	error ("  Trap\n");
      break;

    case ExcCode_FPE:
      if (!quiet)
	error ("  Floating point\n");
      break;

    default:
      if (!quiet)
	error ("Unknown exception: %d\n", CP0_ExCode);
      break;
    }
}

/*This function adds the main thread information to the
  thread table.*/
void initMainThread(){
	threadTable[threadCount] = new threadEntry();
	threadTable[threadCount]->threadID=0;
	strcpy(threadTable[threadCount]->threadName,"main");
	threadTable[threadCount]->threadState=3;
	threadTable[threadCount]->stackAdd=R[29];
	threadTable[threadCount]->pCounter=PC+4;
	threadTable[threadCount]->threadHI=HI;
	threadTable[threadCount]->threadLO=LO;
	memcpy(&(threadTable[threadCount]->registers), &R, sizeof(R) );

    for(int i=0;i<4;++i){
        for(int j=0;j<32;++j){
            threadTable[threadCount]->threadCCR[i][j]=CPR[i][j];
            threadTable[threadCount]->threadCPR[i][j]=CCR[i][j];
        }
    }

	++threadCount;
	currentRuningThread=0;

	joinThreadId=-1;
	joinThreadFlag=0;

  /*For random function*/
    srand(time(NULL));

}

/*Create new thread and add to thread table*/
void threadCreate(){
	int x=0;

	/*Create new thread entry for thread table*/
	while(threadTable[x]!=NULL) x++;
	threadTable[x]=new threadEntry();

	/* Filling the current process to new process table's index */
	char num[20];

	/*Thread name*/
	sprintf(num,"Thread %d", x);
	strcpy(threadTable[x]->threadName,num);

	threadTable[x]->pCounter= R[REG_A0];//thread function address;
	threadTable[x]->threadID=R[REG_A1]; /*Thread ID*/
	threadTable[x]->threadState=1;
	threadTable[x]->stackAdd=R[29];
	memcpy ( &(threadTable[x]->registers), &R, sizeof(R) );

	for(int i=0;i<4;++i){
		for(int j=0;j<32;++j){
			threadTable[x]->threadCCR[i][j]=CPR[i][j];
			threadTable[x]->threadCPR[i][j]=CCR[i][j];
		}
	}
	/*HI and LO retgisters*/
	threadTable[x]->threadHI=HI;
	threadTable[x]->threadLO=LO;

	threadCount+=1;
}

void threadExit(){
	/*Delete thread from thread table*/
	delete(threadTable[currentRuningThread]);
	threadTable[currentRuningThread]=NULL;
	threadCount-=1;

	/*Find new thread from thread table*/
	switchThread();
}

/*Switch thread with the new one*/
void switchThread(){
	int index=0;

	/*Find new thread*/
	int i=currentRuningThread+1;
	while(TRUE){
		if(threadTable[i]!=NULL){
			if(i==0 && joinThreadFlag!=0){
				if(isThreadFinished(joinThreadId)==0){
          i++;
					i=i%100;
					continue;
				}
			}
		index=i;
		break;
    }
    i++;
    i=i%100;
  }

  if (index!=currentRuningThread){
    /*Place the threads in a registers*/
    PC=threadTable[index]->pCounter; //thread function address;
    threadTable[index]->threadState=3;
    R[29]=threadTable[index]->stackAdd;
    memcpy(&R, &(threadTable[index]->registers),sizeof(R));

    for(int i=0;i<4;++i){
      for(int j=0;j<32;++j){
        CPR[i][j]=threadTable[index]->threadCCR[i][j];
        CCR[i][j]=threadTable[index]->threadCPR[i][j];
      }
    }
    /*HI and LO retgisters*/
    HI=threadTable[index]->threadHI;
    LO=threadTable[index]->threadLO;

      // only for producer consumer problem
    // if(currentRuningThread!=0)
    //   R[REG_A0]=producerConsumerArea;

    currentRuningThread=index;
  }
}

/*Updates the current thread informations in the thread table before switch*/
void updateThread(){
	threadTable[currentRuningThread]->pCounter=PC-4;
	threadTable[currentRuningThread]->stackAdd=R[29];
	memcpy(&(threadTable[currentRuningThread]->registers), &R, sizeof(R));

	for(int i=0;i<4;++i){
		for(int j=0;j<32;++j){
			threadTable[currentRuningThread]->threadCCR[i][j]=CPR[i][j];
			threadTable[currentRuningThread]->threadCPR[i][j]=CCR[i][j];
		}
	}
	threadTable[currentRuningThread]->threadHI=HI;
	threadTable[currentRuningThread]->threadLO=LO;

  // only for producer consumer problem
  // if(currentRuningThread!=0)
  //   producerConsumerArea=R[REG_A0];
}

/*For thread_join*/
void joinThread(){
	joinThreadId=R[REG_A0]; /*Take pid as a parameter*/
	joinThreadFlag=1;

	if(isThreadFinished(joinThreadId)==1){
		joinThreadFlag=0;
		return;
	}

	if(threadTable[currentRuningThread]->threadState==3)
	threadTable[currentRuningThread]->threadState=2;

	updateThread();
	switchThread();
	threadTable[currentRuningThread]->threadState=1;
}

/* 
  Check if given thread is finished 
    0 - Not finished
    1 - Finished
*/
int isThreadFinished(int tid){
  for(int i=0;i<100;i++){
    if(threadTable[i]!=NULL && threadTable[i]->threadID==tid) return 0;
  }
  return 1;
}

void mutexLock(){
	int mutexId=R[REG_A0];

  // second and part only for producer consumer
	if(mutexList[mutexId]==0 ){
		mutexList[mutexId]=1;
    // R[REG_A1]=producerConsumerArea;
		threadTable[currentRuningThread]->threadState=3; 
	}
	else{ /*If mutex locked go to the next thread.*/
		PC=PC-4;
		threadTable[currentRuningThread]->threadState=2;
		updateThread();
		switchThread();
		threadTable[currentRuningThread]->threadState=3;
	}
}

void mutexUnlock(){
	int mutexId=R[REG_A0];

	if(mutexList[mutexId]==1){
      mutexList[mutexId]=0;
      // producerConsumerArea=R[REG_A1];
  }

}

